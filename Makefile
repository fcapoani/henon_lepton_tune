henonfort: henon_tune.o tune_comp_average2.o
	g++ henon_tune.o tune_comp_average2.o -o henon_tune -L/usr/lib -lgfortran -O6
	rm *.o

tune_comp_average2.o : tune_comp_average2.f
	gfortran -c tune_comp_average2.f -o tune_comp_average2.o -fallow-argument-mismatch

henon_tune.o : henon_tune.C
	g++ -c henon_tune.C henon_tune.o -O6


clean:
	rm henon_tune *.o

